import axios from "axios";
import { issueAction } from "../Redux/Actions/issueActions";

// API REQUEST
export const apiCallPage3 = (id) => async (dispatch) => {
  try {
    const { data } = await axios.get(
      `https://gitlab.com/api/v4/projects/${id}/issues`
    );
    console.log(data, "My issues");
    dispatch(issueAction(data));
  } catch (error) {
    console.log(error);
  }
};
