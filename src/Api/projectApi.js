import axios from "axios";
import store from "../Redux/store";
import { proAction } from "../Redux/Actions/projectActions";

// API REQUEST
export const apiCallPage2 = () => async (dispatch) => {
  try {
    const userList = store.getState().usersReducer.userList;

    const { data } = await axios.get(
      `https://gitlab.com/api/v4/users/${userList.id}/projects`,
      {}
    );
    console.log(data);
    dispatch(proAction(data));
  } catch (error) {
    console.log(error);
  }
};
