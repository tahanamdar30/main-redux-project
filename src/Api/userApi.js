import axios from "axios";
import store from "../Redux/store";
import { userApiCall } from "../Redux/Actions/userActions";
import { Error2 } from "../Redux/Actions/userActions";
import { LoaderFunc } from "../Redux/Actions/userActions";
// API REQUEST
export const apiCall = () => async (dispatch) => {
  try {
    dispatch(LoaderFunc());
    const token = store.getState().usersReducer.formInput;

    const { data } = await axios.get("https://gitlab.com/api/v4/user", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    console.log(data, "My Data222");

    dispatch(userApiCall(data));
  } catch (error) {
    const errorMsg = error;
    dispatch(Error2());
  }
};
