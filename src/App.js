import React from "react";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Switch, Route } from "react-router-dom";
//screens
import Page3 from "./Components/screens/Page3.js/Page3";
import Page2 from "./Components/screens/page2.js/page2";
import Page1 from "./Components/screens/Page1/Page1";
import { CartPage } from "./Components/screens/cart/cartPage";
function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/page3">
            <Page3 />
          </Route>
          <Route path="/page2">
            <Page2 />
          </Route>
          <Route path="/cart">
            <CartPage />
          </Route>
          <Route path="/">
            <Page1 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
