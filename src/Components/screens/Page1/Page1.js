import BasicTextFields from "./Input";
import React from "react";
import "./Page1.css";
import ButtonAppBar from "../../Nav/nav";

const Page1 = () => {
  return (
    <section className="body">
      <ButtonAppBar />
      <div className="box">
        <br></br>
        <h2>Welcome My Friend , Please Enter your Token :)</h2>
        <BasicTextFields />
      </div>
    </section>
  );
};

export default Page1;
