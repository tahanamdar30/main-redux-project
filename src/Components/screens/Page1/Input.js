import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { useDispatch, useSelector } from "react-redux";
import { Form } from "../../../Redux/Actions/userActions";
import { apiCall } from "../../../Api/userApi";
import { Link } from "react-router-dom";
//Metrial ui Style

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(2),
      width: "25ch",
    },
  },
}));

export default function BasicTextFields() {
  const classes = useStyles();
  const usersReducer = useSelector((state) => state.usersReducer);
  const { formInput } = usersReducer;

  //Redux
  const dispatch = useDispatch();

  //My Handler Functions

  const inputHandler = (e) => {
    dispatch(Form(e.target.value));
  };

  const submit = () => {
    dispatch(apiCall());
    if (!formInput) {
      alert("Opps !!! Please Enter Your Token");
    }
  };

  return (
    <section>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="standard-basic"
          label="Enter Api Token"
          value={formInput}
          onChange={inputHandler}
        />
        <Link to="/cart">
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            startIcon={<CloudUploadIcon />}
            onClick={submit}
          >
            Send Token
          </Button>
        </Link>
      </form>
    </section>
  );
}
