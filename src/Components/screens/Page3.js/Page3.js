import { useSelector } from "react-redux";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import StarBorder from "@material-ui/icons/StarBorder";
import "./Page3.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",

    backgroundColor: "gray",
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const Page3 = () => {
  const classes = useStyles();
  const [open] = React.useState(true);

  const issueReducer = useSelector((state) => state.issuesReducer);
  const { issues } = issueReducer;

  const myTotalEstimate = issues.map((item) => {
    return item.time_stats.time_estimate / 3600;
  });

  const resultEstimate = myTotalEstimate.reduce((a, b) => a + b, 0);

  const myTotalSpent = issues.map((item) => {
    return item.time_stats.total_time_spent / 3600;
  });

  const resultSpent = myTotalSpent.reduce((a, b) => a + b, 0);

  return (
    <div>
      <section style={{ display: "flex" }}>
        <div style={{ width: "50%" }}>
          {issues.map((item) => {
            return (
              <List className={classes.root}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItem button className={classes.nested}>
                      <ListItemIcon>
                        <StarBorder />
                      </ListItemIcon>
                      <ListItemText primary={item.title} />
                      <section>
                        <p>
                          Spnet
                          <span>
                            {item.time_stats.total_time_spent / 3600} h
                          </span>
                        </p>

                        <p>
                          Estimate
                          <span>{item.time_stats.time_estimate / 3600} h</span>
                        </p>
                      </section>
                    </ListItem>
                  </List>
                </Collapse>
              </List>
            );
          })}
        </div>
        <div className="contain" style={{ width: "50%" }}>
          <h1>Total-Spent : {resultSpent} h</h1>
          <h1>Total-Estimate : {resultEstimate} h</h1>
        </div>
      </section>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          marginTop: "100px",
        }}
      ></div>
    </div>
  );
};

export default Page3;
