import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import "./page2.css";
import { apiCallPage3 } from "../../../Api/issuesApi";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    margin: "0 auto",
    top: "20px",
  },
}));

function Page2() {
  const classes = useStyles();

  // const dispatch = useDispatch()
  const projectReducer = useSelector((state) => state.projectsReducer);

  const { projects } = projectReducer;

  const dispatch = useDispatch();

  const api3 = (item) => {
    dispatch(apiCallPage3(item.id));
  };

  return projects.map((item) => {
    return (
      <section className="body">
        <List
          component="nav"
          className={classes.root}
          aria-label="mailbox folders"
        >
          <Link to="/page3" className="Link">
            <ListItem
              button
              onClick={() => {
                api3(item);
              }}
            >
              <ListItemText primary={item.name} />
            </ListItem>
          </Link>
        </List>
      </section>
    );
  });
}

export default Page2;
