import React from "react";
import Cart from "./cart";
import "./cart.css";
export const CartPage = () => {
  return (
    <div className="main">
      <Cart />
    </div>
  );
};
