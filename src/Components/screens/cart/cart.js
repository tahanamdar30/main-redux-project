import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { apiCallPage2 } from "../../../Api/projectApi";
const useStyles = makeStyles({
  root: {
    width: 345,
    margin: "auto",
    height: "400px",
    boxShadow: "1px 4px 3px 4px black",
  },
});

const Cart = () => {
  const classes = useStyles();
  const userReducer = useSelector((state) => state.usersReducer);
  const { userList } = userReducer;

  const dispatch = useDispatch();

  const apiPage2 = () => {
    dispatch(apiCallPage2());
  };

  return (
    <div>
      <div>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              component="img"
              alt={userList.state}
              height="250px"
              width="100%"
              image={userList.avatar_url}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {userList.name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="h3">
                Create at: {userList.created_at}
              </Typography>
            </CardContent>
          </CardActionArea>
          <Link to="/page2">
            <Button
              onClick={apiPage2}
              variant="outlined"
              style={{ backgroundColor: "white" }}
            >
              Next
            </Button>
          </Link>
        </Card>
      </div>
    </div>
  );
};

export default Cart;
