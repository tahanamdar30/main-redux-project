import { PROJECT_ISSUES } from "../Types/types";

export const issueAction = (input) => {
  return {
    type: PROJECT_ISSUES,
    payload: input,
  };
};
