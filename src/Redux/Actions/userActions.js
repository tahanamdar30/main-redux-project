import { USER_API } from "../Types/types";
import { FORM_INPUT } from "../Types/types";

import { LOADER } from "../Types/types";
import { ERROR } from "../Types/types";
//

//
export const userApiCall = (users) => {
  return {
    type: USER_API,
    payload: users,
  };
};

export const Form = (eTarget) => {
  return {
    type: FORM_INPUT,
    payload: eTarget,
  };
};

export const LoaderFunc = () => {
  return { type: LOADER };
};

export const Error2 = (input) => {
  return { type: ERROR, payload: input };
};

