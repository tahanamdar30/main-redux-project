import { PROJECT_API } from "../Types/types";

export const proAction = (input) => {
  return {
    type: PROJECT_API,
    payload: input,
  };
};
