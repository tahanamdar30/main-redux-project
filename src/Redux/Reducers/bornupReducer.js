import { CHART } from "../Types/types";
const initialState = {
  chart: [],
};

export const issueReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_ISSUES:
      return { ...state, issues: action.payload };

    default:
      return state;
  }
};
