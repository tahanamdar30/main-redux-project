import { PROJECT_ISSUES } from "../Types/types";

const initialState = {
  issues: [],
};

export const issueReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_ISSUES:
      return { ...state, issues: action.payload };

    default:
      return state;
  }
};
