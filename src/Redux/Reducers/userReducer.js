import { USER_API } from "../Types/types";
import { FORM_INPUT } from "../Types/types";
import { LOADER } from "../Types/types";
import { ERROR } from "../Types/types";
const initialState = {
  userList: [],
  formInput: "",
  loading: false,
  error: "",
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_API:
      return { ...state, userList: action.payload, loading: false };
    case FORM_INPUT:
      return { ...state, formInput: action.payload };
    case LOADER:
      return { ...state, loading: true };
    case ERROR:
      return { ...state, error: action.payload, loading: false, userList: [] };
    default:
      return state;
  }
};
