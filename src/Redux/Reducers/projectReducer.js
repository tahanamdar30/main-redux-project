import { PROJECT_API } from "../Types/types";

const initialState = {
  projects: [],
};

export const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_API:
      return { ...state, projects: action.payload };

    default:
      return state;
  }
};
