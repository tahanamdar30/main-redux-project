import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { userReducer } from "../Redux/Reducers/userReducer";
import { projectReducer } from "./Reducers/projectReducer";
import { issueReducer } from "../Redux/Reducers/issuesReducer";
const reducer = combineReducers({
  usersReducer: userReducer,
  projectsReducer: projectReducer,
  issuesReducer: issueReducer,
});

const middlewere = [thunk];

const store = createStore(
  reducer,

  applyMiddleware(...middlewere)
);

export default store;
